package nl.utwente.di.toFahrenheit;

public class Quoter {
    public double getFahrenheitValue(String input) {
        double result = Double.parseDouble(input);
        return (result*1.8 +32);

    }
}
