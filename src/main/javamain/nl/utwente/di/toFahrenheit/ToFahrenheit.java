package nl.utwente.di.toFahrenheit;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class ToFahrenheit extends HttpServlet {
 	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Quoter quoter;

    public void init() throws ServletException {
    	quoter = new Quoter();
    }

  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "Fahrenheit Value";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celsius degree: " +
                   request.getParameter("celc") + "\n" +
                "  <P>Fahrenheit degree: " +
                   Double.toString(quoter.getFahrenheitValue(request.getParameter("celc"))) +
                "<form action=\"./\">\n" +
                "    <input type=\"submit\" value=\"Return\" />\n" +
                "</form>" +
                "</BODY></HTML>");
  }



}
