package javamain.nl.utwente.di.bookQuote;



import nl.utwente.di.toFahrenheit.Quoter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestQuoter {

    /** * Tests the Quoter*/
    @Test
    public void testBook1() throws Exception{
        Quoter quoter = new Quoter();
        double price = quoter.getFahrenheitValue("0");
        Assertions.assertEquals(32.0, price, 0.0, "Value of Celcius 0");
    }

}
